/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import './styles/app.css';

// start the Stimulus application
import './bootstrap';

(function() {
    const selectWorkplace = document.querySelector('#form_workplace');
    selectWorkplace.addEventListener('change', (event) => {
        let selectValue = parseInt(event.target.value);
        hideGroups();
        selectedInput(selectValue);

    });

    document.querySelector('form').addEventListener("submit", function (e) {
        if (parseInt(e.target.querySelector("#form_workplace").value) === 0) {
            e.preventDefault();
            alert("Wybierz stanowisko");
            return false;
        }
        return true;
    });

    let selectedVal = parseInt(selectWorkplace.value);
    if (selectedVal !== 0) {
        selectedInput(selectedVal);
    }
})();

function hideGroups() {
    let listGroup = document.querySelectorAll(".hidden-inputs-group");
    [].forEach.call(listGroup, function(div) {
        let inputsInDiv = div.querySelectorAll('input');
        [].forEach.call(inputsInDiv, function(input) {
            disabledInput(input);
        });
        div.classList.remove('hidden-inputs-group_show');
    });
}


function enabledInput(input) {
    if (input.getAttribute("disabled") === "disabled") {
        input.removeAttribute("disabled");
    }
}
function disabledInput(input) {
    if (input.getAttribute("disabled") !== "disabled") {
        input.setAttribute("disabled", "disabled");
    }
}

function selectedInput(selectValue) {
    switch (selectValue) {
        case 1:
            let testerGroup = document.querySelector("#tester_group");
            [].forEach.call(testerGroup.querySelectorAll('input'), function(input) {
                enabledInput(input);
            });
            testerGroup.classList.add('hidden-inputs-group_show');
            break;
        case 2:
            let developerGroup = document.querySelector("#developer_group");
            [].forEach.call(developerGroup.querySelectorAll('input'), function(input) {
                enabledInput(input);
            });
            developerGroup.classList.add('hidden-inputs-group_show');
            break;
        case 3:
            let pmGroup = document.querySelector("#pm_group");
            [].forEach.call(pmGroup.querySelectorAll('input'), function(input) {
                enabledInput(input);
            });
            pmGroup.classList.add('hidden-inputs-group_show');
            break;
        default:
            hideGroups();
            break;
    }
}