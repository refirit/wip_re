<?php

namespace App\Controller;

use App\Entity\Accounts;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;

class AdminPageController extends AbstractController
{
    private $workplaces = [
        1 => "Tester",
        2 => "Developer",
        3 => "Project Manager"
    ];

    private $skills1 = [
        1 => "Systemy Testujące",
        2 => "Systemy Raportujące",
        3 => "Zna Selenium",
    ];

    private $skills2 = [
        1 => "Środowiska IDE",
        2 => "Języki programowania",
        3 => "Zna MySQL",
        ];

    private $skills3 = [
        1 => "Metodologie Prowadzenia Projektów",
        2 => "Systemy Raportujące",
        3 => "Zna Scrum",
    ];

    #[Route('/admin', name: 'app_admin_page')]
    public function index(ManagerRegistry $doctrine): Response
    {
        $allAccounts = $doctrine->getRepository(Accounts::class);

        return $this->render('admin_page/index.html.twig', [
            'all' => $allAccounts->findBy(["is_active" => 1])
        ]);
    }

    #[Route('/admin/show/{id}', name: 'app_record_show', methods: "GET")]
    public function show($id, ManagerRegistry $doctrine): Response
    {
        $allAccounts = $doctrine->getRepository(Accounts::class);
        $record = $allAccounts->find($id);
        if (!$record) {
            return $this->redirectToRoute("app_admin_page");
        }
        return $this->render('admin_page/show.html.twig', [
            'record' => $record,
            'workplaces' => $this->workplaces,
            'skills' => [
                1 => $this->skills1,
                2 => $this->skills2,
                3 => $this->skills3
            ]
        ]);
    }

    #[Route('/admin/edit/{id}', name: 'app_record_edit')]
    public function edit($id, Request $request, ManagerRegistry $doctrine): Response
    {
        $allAccounts = $doctrine->getRepository(Accounts::class);
        $record = $allAccounts->find($id);
        $form = $this->buildForm($record);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $name = $form->getData()["first_name"];
            $surname = $form->getData()["surname"];
            $email = $form->getData()["email"];
            $desc = $form->getData()["desc"];
            $workplace = $form->getData()["workplace"];
            $skill1 = $skill2 = $skill3 = '';
            match ($workplace) {
                1 => [
                    $skill1 = $form->getData()["tester_skill1"],
                    $skill2 = $form->getData()["tester_skill2"],
                    $skill3 = $form->getData()["tester_skill3"],
                ],
                2 => [
                    $skill1 = $form->getData()["developer_skill1"],
                    $skill2 = $form->getData()["developer_skill2"],
                    $skill3 = $form->getData()["developer_skill3"],
                ],
                3 => [
                    $skill1 = $form->getData()["pm_skill1"],
                    $skill2 = $form->getData()["pm_skill2"],
                    $skill3 = $form->getData()["pm_skill3"],
                ],
            };
            $entityManager = $doctrine->getManager();
            $account = $entityManager->getRepository(Accounts::class)->find($id);
            $account->setFirstName($name);
            $account->setSurname($surname);
            $account->setEmail($email);
            $account->setDescription($desc);
            $account->setWorkplace($workplace);
            $account->setSkill1($skill1);
            $account->setSkill2($skill2);
            $account->setSkill3($skill3);
            $account->setIsActive(1);

            $entityManager->persist($account);
            $entityManager->flush();
            return $this->redirectToRoute("app_admin_page");
        }
        return $this->render('admin_page/edit.html.twig', [
            'form' => $form->createView(),
            'record' => $record,
            'workplaces' => $this->workplaces,
            'skills' => [
                1 => $this->skills1,
                2 => $this->skills2,
                3 => $this->skills3
            ]
        ]);
    }

    #[Route('/admin/delete/{id}', name: 'app_record_delete', methods: "GET")]
    public function delete($id, ManagerRegistry $doctrine): RedirectResponse
    {
        $entityManager = $doctrine->getManager();
        $account = $entityManager->getRepository(Accounts::class)->find($id);
        $account->setIsActive(0);
        $entityManager->persist($account);
        $entityManager->flush();
        return $this->redirectToRoute("app_admin_page");
    }

    private function buildForm($record)
    {

        $form = $this->createFormBuilder()
            ->add('first_name', TextType::class, [
                'required' => true,
                'label' => 'Imię',
                'data' => $record->getFirstName(),
                'constraints' => [
                    new NotBlank(['message' => 'To pole jest wymagane']),
                ]
            ])
            ->add('surname', TextType::class, [
                'required' => true,
                'label' => 'Nazwisko',
                'data' => $record->getSurname(),
                'constraints' => [
                    new NotBlank(['message' => 'To pole jest wymagane']),
                ]
            ])
            ->add('email', EmailType::class, [
                'required' => true,
                'label' => 'E-mail',
                'data' => $record->getEmail(),
                'constraints' => [
                    new NotBlank(['message' => 'To pole jest wymagane']),
                    new Email(['message' => 'Wprowadzony adres e-mail nie jest poprawny']),
                ]
            ])
            ->add('desc', TextareaType::class, [
                'required' => false,
                'data' => $record->getDescription(),
                'label' => 'Opis'
            ])
            ->add('workplace', ChoiceType::class, [
                'required' => true,
                'label' => 'Stanowisko',
                'data' => $record->getWorkplace(),
                'choices'  => [
                    'wybierz' => 0,
                    'Tester' => 1,
                    'Developer' => 2,
                    'Project Manager' => 3,
                ]
            ])
            ->add('tester_skill1', TextType::class, [
                'required' => false,
                'data' => $record->getWorkplace() === 1 ? $record->getSkill1() : null,
                'label' => 'Systemy Testujące'
            ])
            ->add('tester_skill2', TextType::class, [
                'required' => false,
                'data' => $record->getWorkplace() === 1 ? $record->getSkill2() : null,
                'label' => 'Systemy Raportujące'
            ])
            ->add('tester_skill3', CheckboxType::class, [
                'required' => false,
                'data' => $record->getWorkplace() === 1 ? $record->getSkill3() : null,
                'label' => 'Tak'
            ])
            ->add('developer_skill1', TextType::class, [
                'required' => false,
                'data' => $record->getWorkplace() === 2 ? $record->getSkill1() : null,
                'label' => 'Środowiska IDE'
            ])
            ->add('developer_skill2', TextType::class, [
                'required' => false,
                'data' => $record->getWorkplace() === 2 ? $record->getSkill2() : null,
                'label' => 'Języki programowania'
            ])
            ->add('developer_skill3', CheckboxType::class, [
                'required' => false,
                'data' => $record->getWorkplace() === 2 ? $record->getSkill3() : null,
                'label' => 'Tak'
            ])
            ->add('pm_skill1', TextType::class, [
                'required' => false,
                'data' => $record->getWorkplace() === 3 ? $record->getSkill1() : null,
                'label' => 'Metodologie Prowadzenia Projektów'
            ])
            ->add('pm_skill2', TextType::class, [
                'required' => false,
                'data' => $record->getWorkplace() === 3 ? $record->getSkill2() : null,
                'label' => 'Systemy Raportowe'
            ])
            ->add('pm_skill3', CheckboxType::class, [
                'required' => false,
                'data' => $record->getWorkplace() === 3 ? $record->getSkill3() : null,
                'label' => 'Tak'
            ])
            ->add('save', SubmitType::class, [
                'label' => 'Wyślij',
            ])
            ->getForm();

        return $form;
    }
}
