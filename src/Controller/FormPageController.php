<?php

namespace App\Controller;

use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;

use App\Entity\Accounts;

class FormPageController extends AbstractController
{
    #[Route('/', name: 'app_form_page')]
    public function index(Request $request, ManagerRegistry $doctrine): Response
    {
        $form = $this->buildForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $name = $form->getData()["first_name"];
            $surname = $form->getData()["surname"];
            $email = $form->getData()["email"];
            $desc = $form->getData()["desc"];
            $workplace = $form->getData()["workplace"];
            $skill1 = $skill2 = $skill3 = '';
            match ($workplace) {
                1 => [
                    $skill1 = $form->getData()["tester_skill1"],
                    $skill2 = $form->getData()["tester_skill2"],
                    $skill3 = $form->getData()["tester_skill3"],
                ],
                2 => [
                    $skill1 = $form->getData()["developer_skill1"],
                    $skill2 = $form->getData()["developer_skill2"],
                    $skill3 = $form->getData()["developer_skill3"],
                ],
                3 => [
                    $skill1 = $form->getData()["pm_skill1"],
                    $skill2 = $form->getData()["pm_skill2"],
                    $skill3 = $form->getData()["pm_skill3"],
                ],
            };
            $entityManager = $doctrine->getManager();
            $accounts = new Accounts();
            $accounts->setFirstName($name);
            $accounts->setSurname($surname);
            $accounts->setEmail($email);
            $accounts->setDescription($desc);
            $accounts->setWorkplace($workplace);
            $accounts->setSkill1($skill1);
            $accounts->setSkill2($skill2);
            $accounts->setSkill3($skill3);
            $accounts->setIsActive(1);

            $entityManager->persist($accounts);
            $entityManager->flush();
            return $this->redirectToRoute("app_success_page");
        }

        return $this->render('form_page/index.html.twig', [
            'form' => $form->createView()
        ]);
    }

    private function buildForm()
    {

        $form = $this->createFormBuilder()
            ->add('first_name', TextType::class, [
                'required' => true,
                'label' => 'Imię',
                'constraints' => [
                    new NotBlank(['message' => 'To pole jest wymagane']),
                ]
            ])
            ->add('surname', TextType::class, [
                'required' => true,
                'label' => 'Nazwisko',
                'constraints' => [
                    new NotBlank(['message' => 'To pole jest wymagane']),
                ]
            ])
            ->add('email', EmailType::class, [
                'required' => true,
                'label' => 'E-mail',
                'constraints' => [
                    new NotBlank(['message' => 'To pole jest wymagane']),
                    new Email(['message' => 'Wprowadzony adres e-mail nie jest poprawny']),
                ]
            ])
            ->add('desc', TextareaType::class, [
                'required' => false,
                'label' => 'Opis'
            ])
            ->add('workplace', ChoiceType::class, [
                'required' => true,
                'label' => 'Stanowisko',
                'choices'  => [
                    'wybierz' => 0,
                    'Tester' => 1,
                    'Developer' => 2,
                    'Project Manager' => 3,
                ]
            ])
            ->add('tester_skill1', TextType::class, [
                'required' => false,
                'label' => 'Systemy Testujące'
            ])
            ->add('tester_skill2', TextType::class, [
                'required' => false,
                'label' => 'Systemy Raportujące'
            ])
            ->add('tester_skill3', CheckboxType::class, [
                'required' => false,
                'label' => 'Tak'
            ])
            ->add('developer_skill1', TextType::class, [
                'required' => false,
                'label' => 'Środowiska IDE'
            ])
            ->add('developer_skill2', TextType::class, [
                'required' => false,
                'label' => 'Języki programowania'
            ])
            ->add('developer_skill3', CheckboxType::class, [
                'required' => false,
                'label' => 'Tak'
            ])
            ->add('pm_skill1', TextType::class, [
                'required' => false,
                'label' => 'Metodologie Prowadzenia Projektów'
            ])
            ->add('pm_skill2', TextType::class, [
                'required' => false,
                'label' => 'Systemy Raportowe'
            ])
            ->add('pm_skill3', CheckboxType::class, [
                'required' => false,
                'label' => 'Tak'
            ])
            ->add('save', SubmitType::class, [
                'label' => 'Wyślij',
            ])
            ->getForm();

        return $form;
    }
}
